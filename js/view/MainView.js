/**
 * Created by anastasiya.dmitrenko on 3/7/2016.
 */
var MainView = function(rootElement){
    this._root = rootElement;

    this.init = function(){
        this.createInput();
        this.createMaze();
    };

    this.createInput = function() {
        this.inputView = document.createElement("div");
        this.inputView.id = "InputId";
        this.inputView.className = "input-view";
        this._root.appendChild(this.inputView);
        var label = document.createElement("span");
        label.innerHTML = "Input size(max 900*700): ";
        label.style.marginRight = "10px";
        this.inputView.appendChild(label);
        this.initInput("width", 900);
        this.initInput("height", 700);
        this.addShowButton();
    };

    this.initInput = function(name, maxValue){
        var input = document.createElement("input");
        input.id = name + "Id";
        input.type = "text";
        input.style.marginRight = "10px";
        input.onkeydown = function(event){
            var code = event.keyCode;
            if((code < "0".charCodeAt(0) || code > "9".charCodeAt(0)) && code !== 8){
                event.preventDefault();
            }
            var realValue = this.value;
            if(this.selectionStart !== this.selectionEnd && this.selectionStart >= 0){
                realValue = this.value.replace(this.value.substring(this.selectionStart,
                    this.selectionEnd), String.fromCharCode(code));
            } else {
                realValue += String.fromCharCode(code);
            }
            realValue = parseInt(realValue);
            if(realValue > maxValue){
                this.value = maxValue;
                event.preventDefault();
            }
        };
        this.inputView.appendChild(input);
    };

    this.addShowButton = function(){
        var button = document.createElement("button");
        button.className = "input-button";
        button.innerHTML = "Show";
        button.id = "ShowButton";
        this.inputView.appendChild(button);
        button.addEventListener("click", this.onShowButtonClick.bind(this));
    };

    this.onShowButtonClick = function(){
        this.mazeGenerator.clearMaze();
        this.mazeGenerator.drawMaze(this.getWidth(),this.getHeight());
    };

    this.getWidth = function(){
        return document.getElementById("widthId").value;
    };

    this.getHeight = function(){
        return document.getElementById("heightId").value;
    };

    this.createMaze = function() {
        this.mazeView = document.createElement("div");
        this.mazeView.id = "MazeId";
        this.mazeView.className = "maze-view";
        this._root.appendChild(this.mazeView);
        var canvas = document.createElement("canvas");
        this.mazeView.appendChild(canvas);
        this.mazeGenerator = new MazeGenerator(canvas);
    };
};