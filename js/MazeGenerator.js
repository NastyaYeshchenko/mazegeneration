/**
 * Created by anastasiya.dmitrenko on 3/31/2016.
 */
var MazeGenerator = function(canvas) {
    this.canvas = canvas;
    this.canvas.width = 900;
    this.canvas.height = 700;

    this.drawMaze = function(width, height) {
        this.width = width;
        this.height = height;
        var N = 0,
            S = 1,
            W = 2,
            E = 3;

        var cellSize = 4,
            cellSpacing = 4,
            cellWidth = Math.floor((this.width - cellSpacing) / (cellSize + cellSpacing)),
            cellHeight = Math.floor((this.height - cellSpacing) / (cellSize + cellSpacing)),
            cells = new Array(cellWidth * cellHeight),
            frontier = [];

        var context = this.canvas.getContext("2d");

        context.translate(Math.round((900-this.width)/2), 0);
        context.fillRect(0, 0, this.width, this.height);

        context.translate(
            Math.round((this.width - cellWidth * cellSize - (cellWidth + 1) * cellSpacing) / 2),
            Math.round((this.height - cellHeight * cellSize - (cellHeight + 1) * cellSpacing) / 2)
        );

        context.fillStyle = "white";

        var start = (cellHeight - 1) * cellWidth;
        cells[start] = 0;
        fillCell(start);
        frontier.push({index: start, direction: N});
        frontier.push({index: start, direction: E});

        draw();

        function draw() {
            if (!exploreFrontier()) {
                setTimeout(draw, 1);
            }
        }

        function exploreFrontier() {
            if ((edge = popRandom(frontier)) == null) return true;

            var edge,
                i0 = edge.index,
                d0 = edge.direction,
                i1 = i0 + (d0 === N ? -cellWidth : d0 === S ? cellWidth : d0 === W ? -1 : +1),
                x0 = i0 % cellWidth,
                y0 = i0 / cellWidth | 0,
                x1,
                y1,
                d1,
                open = cells[i1] == null;

            context.fillStyle = open ? "white" : "black";
            switch (d0){
                case N :
                    fillSouth(i1);
                    x1 = x0;
                    y1 = y0 - 1;
                    d1 = S;
                    break;
                case S :
                    fillSouth(i0);
                    x1 = x0;
                    y1 = y0 + 1;
                    d1 = N;
                    break;
                case W :
                    fillEast(i1);
                    x1 = x0 - 1;
                    y1 = y0;
                    d1 = E;
                    break;
                case E:
                    fillEast(i0);
                    x1 = x0 + 1;
                    y1 = y0;
                    d1 = W;
                    break;
            }

            if (open) {
                fillCell(i1);
                cells[i0] |= d0;
                cells[i1] |= d1;
                context.fillStyle = "red";
                if (y1 > 0 && cells[i1 - cellWidth] == null){
                    fillSouth(i1 - cellWidth);
                    frontier.push({
                        index: i1,
                        direction: N
                    });
                }
                if (y1 < cellHeight - 1 && cells[i1 + cellWidth] == null){
                    fillSouth(i1);
                    frontier.push({
                        index: i1,
                        direction: S
                    });
                }
                if (x1 > 0 && cells[i1 - 1] == null){
                    fillEast(i1 - 1);
                    frontier.push({
                        index: i1,
                        direction: W
                    });
                }
                if (x1 < cellWidth - 1 && cells[i1 + 1] == null){
                    fillEast(i1);
                    frontier.push({
                        index: i1,
                        direction: E
                    });
                }
            }
        }

        function fillCell(index) {
            var i = index % cellWidth,
                j = index / cellWidth | 0;
            context.fillRect(i * cellSize + (i + 1) * cellSpacing, j * cellSize + (j + 1) * cellSpacing, cellSize, cellSize);
        }

        function fillEast(index) {
            var i = index % cellWidth,
                j = index / cellWidth | 0;
            context.fillRect((i + 1) * (cellSize + cellSpacing), j * cellSize + (j + 1) * cellSpacing, cellSpacing, cellSize);
        }

        function fillSouth(index) {
            var i = index % cellWidth,
                j = index / cellWidth | 0;
            context.fillRect(i * cellSize + (i + 1) * cellSpacing, (j + 1) * (cellSize + cellSpacing), cellSize, cellSpacing);
        }

        function popRandom(array) {
            if (!array.length) return;
            var n = array.length,
                i = Math.random() * n | 0,
                t;
            t = array[i];
            array[i] = array[n - 1];
            array[n - 1] = t;
            return array.pop();
        }
    };

    this.clearMaze = function(){
        var context = this.canvas.getContext("2d");
        context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    };
};